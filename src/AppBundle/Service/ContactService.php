<?php

namespace AppBundle\Service;

use AppBundle\Form\ContactType;
use Symfony\Component\Form\FormFactory;

class ContactService
{
    /** @var FormFactory */
    private $formFactory;

    public function __construct(FormFactory $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    public function getForm()
    {
        return $this->formFactory->create(ContactType::class, null, []);
    }
}
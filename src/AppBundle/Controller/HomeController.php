<?php

// src/AppBundle/HomeController.php

namespace AppBundle\Controller;

use AppBundle\Entity\Contact;
use AppBundle\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends Controller
{
    public function homeAction()
    {
        $form = $this->get('appbundle.service.contact')->getForm();

        $parameters = ['form' => $form->createView()];

        return $this->render('AppBundle:Home:home.html.twig', $parameters);
    }

    public function contactAction(Request $request)
    {
        $form = $this->get('appbundle.service.contact')->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var Contact $contact */
            $contact = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($contact);
            $em->flush();

            return new Response('Thanks you for your message, we\'ll respond as soon as possible.');
        }

        $parameters = ['contactForm' => $form->createView()];

        return $this->render('@App/forms/contact.html.twig', $parameters);
    }
}